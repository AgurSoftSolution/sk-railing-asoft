            <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-W5VF5W4');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/site/main.css') }}">

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W5VF5W4"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="app">
    <header class="sticked">
        <div class="container">
            <div class="row flex-center d-none d-xl-flex">
                <span class="logo"><a href="/">SK&amp;RAILING</a></span>
                <menu>
                    <a href="#main">ГЛАВНАЯ</a>
                    <a href="#our-products">ПРОДУКЦИЯ</a>
                    <a href="#our-service">УСЛУГИ</a>
                    <a href="#clients">ОТЗЫВЫ</a>
                    <a href="#contacts">КОНТАКТЫ</a>
                </menu>
                <span class="tel"><a href="tel:+380970775706">+38 (097) 077-57-06</a></span>
            </div>

            <div class="d-xl-none small">
                <div class="row d-none d-md-flex flex-center">
                    <div class="col-md-6"><span id="logo"><a href="/">SK&amp;RAILING</a></span></div>
                    <div class="col-md-6 align-right">
                        <span class="tel"><a href="tel:+74957322984">+38 (097) 077-57-06</a></span>
                    </div>
                </div>
                <menu class="row d-none d-md-block">
                    <div class="col-md-12">
                        <a href="#main">ГЛАВНАЯ</a>
                        <a href="#our-products">ПРОДУКЦИЯ</a>
                        <a href="#our-service">УСЛУГИ</a>
                        <a href="#clients">ОТЗЫВЫ</a>
                        <a href="#contacts">КОНТАКТЫ</a>
                    </div>
                </menu>

                <nav class="navbar navbar-expand-xl navbar-dark d-sm-none">
                    <span class="logo"><a href="/">SK&amp;RAILING</a></span>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#main">ГЛАВНАЯ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#our-products">ПРОДУКЦИЯ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#our-service">УСЛУГИ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#clients">ОТЗЫВЫ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#contacts">КОНТАКТЫ</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <!--<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                SK&RAILING
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav   m-auto   mr-auto" style="">
                    <li class=""><a class="nav-link" href="{{route('home')}}">Главная</a></li>
                    <li class=""><a class="nav-link" href="{{route('products')}}">Продукция</a></li>
                    <li class=""><a class="nav-link" href="">Услуги</a></li>
                    <li class=""><a class="nav-link" href="">Отзывы</a></li>
                    <li class=""><a class="nav-link" href="">Контакты</a></li>
                </ul>

                <ul class="navbar-nav ml-auto">
                    <li><a href="tel:+496170961709" class="Blondie">+7(495)732-29-84</a></li>
                </ul>
            </div>
        </div>
    </nav>-->

    <div class="container py-5">
        <div class="row">
            <div class="col-md-6">
                <h2 class="text-uppercase">Нержавеюшие перила</h2>
                <p >Компания «SK&Railing» — как производитель гарантирует доступную цену и качество
                    нержавеющих
                    перил</p>
            </div>
            <div class="col-md-6">
                @yield('description')
            </div>
        </div>
    </div>


    <div class="container">
        @yield('breadcrumbs')
    </div>
    <main class="py-4">
        @yield('content')
    </main>
</div>

<div id="to-top" class="d-none d-xl-block" href="#main">
    <i class="fa fa-angle-up"></i><br>
    наверх
</div>
<footer class="d-xl-flex">
    <div class="year">2012 - 2018 sk-railing.com.ua</div>
    <div class="logo"><a href="/">SK&amp;RAILING</a></div>
    <div class="phone"><a href="tel:+74957322984">+38 (097) 077-57-06</a></div>
</footer>
</body>
</html>
