<!doctype html>
<html>
<head>
    <meta charset="utf-8">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>sk railing</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/site/main.css') }}">
</head>
<body>
<div id="app">
<header>
    <div class="container">
        <div class="row flex-center d-none d-xl-flex">
            <span class="logo"><a href="/">SK&RAILING</a></span>
            <menu>
                <a href="#main">ГЛАВНАЯ</a>
                <a href="#our-products">ПРОДУКЦИЯ</a>
                <a href="#our-service">УСЛУГИ</a>
                <a href="#clients">ОТЗЫВЫ</a>
                <a href="#contacts">КОНТАКТЫ</a>
            </menu>
            <span class="tel"><a href="tel:+380970775706">+38 (097) 077-57-06</a></span>
        </div>

        <div class="d-xl-none small">
            <div class="row d-none d-md-flex flex-center">
                <div class="col-md-6"><span id="logo"><a href="/">SK&RAILING</a></span></div>
                <div class="col-md-6 align-right">
                    <span class="tel"><a href="tel:+74957322984">+38 (097) 077-57-06</a></span>
                </div>
            </div>
            <menu class="row d-none d-md-block">
                <div class="col-md-12">
                    <a href="#main">ГЛАВНАЯ</a>
                    <a href="#our-products">ПРОДУКЦИЯ</a>
                    <a href="#our-service">УСЛУГИ</a>
                    <a href="#clients">ОТЗЫВЫ</a>
                    <a href="#contacts">КОНТАКТЫ</a>
                </div>
            </menu>

            <nav class="navbar navbar-expand-xl navbar-dark d-sm-none">
                <span class="logo"><a href="/">SK&RAILING</a></span>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#main">ГЛАВНАЯ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#our-products">ПРОДУКЦИЯ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#our-service">УСЛУГИ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#clients">ОТЗЫВЫ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contacts">КОНТАКТЫ</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>

<section id="main">
    <div class="container">
        <div class="main-title">
            <div class="row">
                <div class="col-md-12">СТАЛЬНЫЕ  КОНСТРУКЦИИ</div>
            </div>
            <div class="row">
                <div class="col-md-12">& ПЕРИЛА</div>
            </div>
        </div>

        <div class="callback-wrapper flex-center">
            <div class="callback flex-center">
                <div>
                    <span class="title">Оставте ваш номер и мы перезвоним вам в течении часа</span>
                    <form class="callback-form">
                        <input class="phone-input" type="text" placeholder="ВАШ НОМЕР ТЕЛЕФОНА"/>
                        <button type="submit">ПЕРЕЗВОНИТЬ</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="flex-center d-md-none">
            <div class="phone-form">
                <span>Или позвоните по номеру:</span>
                <span class="phone"><a href="tel:+380970775706">+38 (097) 077-57-06</a></span>
            </div>
        </div>
    </div>
</section>

<section id="greeting">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 wow slideInLeft">
                <span class="name">SK&Railing</span>
                <div class="three-dot">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <div class="text">
                    это наша компания, наше детище которое развивали и растили своей стопроцентной отдачей. Именно поэтому мы ценим наших клиентов, обеспечивая индивидуальный  подход и внимание каждому из них! По этой же причине изготавливаем продукцию из высококачественного металла, хорошее сырьё-залог надежной металлоконструкции!
                </div>
            </div>
            <div class="col-md-6 d-none d-xl-flex">
                <img class="wow slideInRight" src="{{ asset('images/home-page/greeting-photo.jpg') }}">
            </div>
        </div>
    </div>
</section>

<section id="our-products">
    <div class="container">
        <div class="row">
            <div class="col-md-12"><h1>НАШИ ИЗДЕЛИЯ</h1></div>
        </div>
        <div class="tiles d-none d-xl-block">
            <div class="d-flex flex-row">
                <div class="item wow fadeInLeft" style="background: #232027;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/railing-icon.png') }}">
                    </div>
                    <div class="title">
                        Нержавеющие <br />
                        перила
                    </div>
                </div>
                <div class="item wow fadeInUp" style="background: #1d2d34;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/fences-icon.png') }}">
                    </div>
                    <div class="title">
                        ограждения
                    </div>
                </div>
                <div class="item wow fadeInRight" style="background: #201726;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/handrails-icon.png') }}">
                    </div>
                    <div class="title">
                        поручни
                    </div>
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="item wow fadeInLeft" style="background: #3e2323;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/flagpolse-and-pylones-icon.png') }}">
                    </div>
                    <div class="title">
                        Флагштоки и <br />
                        пилоны
                    </div>
                </div>
                <div class="item wow fadeIn" style="background: #2a3d28;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/stairs-for-pool-icon.png') }}">
                    </div>
                    <div class="title">
                        Лестници для <br />
                        бассейна
                    </div>
                </div>
                <div class="item wow fadeInRight" style="background: #3c3d28;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/door-handles-and-lattice-icon.png') }}">
                    </div>
                    <div class="title">
                        дверные ручки <br />
                        решетки
                    </div>
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="item wow fadeInLeft" style="background: #1d2d34;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/arc-for-cars-icon.png') }}">
                    </div>
                    <div class="title">
                        дуги для авто
                    </div>
                </div>
                <div class="item wow fadeInDown" style="background: #232027;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/handrails-icon.png') }}">
                    </div>
                    <div class="title">
                        полотенцесушители
                    </div>
                </div>
                <div class="item wow fadeInRight" style="background: #201726;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/visors-and-cornices-icon.png') }}">
                    </div>
                    <div class="title">
                        козырьки и <br />
                        карнизы
                    </div>
                </div>
            </div>
        </div>

        <div class="tiles d-xl-none">
            <div class="row">
                <div class="item col-6 wow fadeInLeft" style="background: #232027;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/railing-icon.png') }}">
                    </div>
                    <div class="title">
                        Нержавеющие <br />
                        перила
                    </div>
                </div>
                <div class="item col-6 wow fadeInRight" style="background: #1d2d34;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/fences-icon.png') }}">
                    </div>
                    <div class="title">
                        ограждения
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="item col-6 wow fadeInLeft" style="background: #201726;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/handrails-icon.png') }}">
                    </div>
                    <div class="title">
                        поручни
                    </div>
                </div>
                <div class="item col-6 wow fadeInRight" style="background: #3e2323;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/flagpolse-and-pylones-icon.png') }}">
                    </div>
                    <div class="title">
                        Флагштоки и <br />
                        пилоны
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="item col-6 wow fadeInLeft" style="background: #2a3d28;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/stairs-for-pool-icon.png') }}">
                    </div>
                    <div class="title">
                        Лестници для <br />
                        бассейна
                    </div>
                </div>
                <div class="item col-6 wow fadeInRight" style="background: #3c3d28;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/door-handles-and-lattice-icon.png') }}">
                    </div>
                    <div class="title">
                        дверные ручки <br />
                        решетки
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="item col-6 wow fadeInLeft" style="background: #1d2d34;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/arc-for-cars-icon.png') }}">
                    </div>
                    <div class="title">
                        дуги для авто
                    </div>
                </div>
                <div class="item col-6 wow fadeInRight" style="background: #232027;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/handrails-icon.png') }}">
                    </div>
                    <div class="title">
                        полотенцесушители
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="item col-6 wow fadeInLeft" style="background: #201726;">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/visors-and-cornices-icon.png') }}">
                    </div>
                    <div class="title">
                        козырьки и <br />
                        карнизы
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="our-service">
    <div class="container">
        <div class="row">
            <div class="col-md-12"><h1>НАШИ УСЛУГИ</h1></div>
        </div>

        <div class="tiles d-none d-xl-block">
            <div class="d-flex flex-row">
                <div class="item wow slideInRight">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/service-icons/izdeliya-pod-zakaz.jpg') }}">
                    </div>
                    <div class="title">
                        изделия под<br />
                        заказ
                    </div>
                </div>
                <div class="item wow slideInUp">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/service-icons/vosstanovlenie-poverhnostej.jpg') }}">
                    </div>
                    <div class="title">
                        восстановление<br />
                        поверхностей
                    </div>
                </div>
                <div class="item wow slideInLeft">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/service-icons/restavraciya-peril.jpg') }}">
                    </div>
                    <div class="title">
                        реставрация<br />
                        перил
                    </div>
                </div>
            </div>
            <div class="d-flex flex-row">
                <div class="item wow slideInRight">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/service-icons/remont-izdelij-iz-nerzhavejki.jpg') }}">
                    </div>
                    <div class="title">
                        ремонт изделий<br />
                        из нержавейки
                    </div>
                </div>
                <div class="item wow slideInUp">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/service-icons/dizajn-i-proekt.jpg') }}">
                    </div>
                    <div class="title">
                        дизайн и <br />
                        проект
                    </div>
                </div>
                <div class="item wow slideInLeft">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/service-icons/konsultaciya-specialista.jpg') }}">
                    </div>
                    <div class="title">
                        Консультация <br />
                        специалиста
                    </div>
                </div>
            </div>
        </div>

        <div class="tiles d-xl-none">
            <div class="row">
                <div class="item col-6 wow slideInLeft">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/service-icons/izdeliya-pod-zakaz.jpg') }}">
                    </div>
                    <div class="title">
                        изделия под<br />
                        заказ
                    </div>
                </div>
                <div class="item col-6 wow slideInRight">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/service-icons/vosstanovlenie-poverhnostej.jpg') }}">
                    </div>
                    <div class="title">
                        восстановление<br />
                        поверхностей
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="item col-6 wow slideInLeft">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/service-icons/restavraciya-peril.jpg') }}">
                    </div>
                    <div class="title">
                        реставрация<br />
                        перил
                    </div>
                </div>
                <div class="item col-6 wow slideInRight">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/service-icons/remont-izdelij-iz-nerzhavejki.jpg') }}">
                    </div>
                    <div class="title">
                        ремонт изделий<br />
                        из нержавейки
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="item col-6 wow slideInLeft">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/service-icons/dizajn-i-proekt.jpg') }}">
                    </div>
                    <div class="title">
                        дизайн и <br />
                        проект
                    </div>
                </div>
                <div class="item col-6 wow slideInRight">
                    <div class="icon">
                        <img src="{{ asset('images/home-page/service-icons/konsultaciya-specialista.jpg') }}">
                    </div>
                    <div class="title">
                        Консультация <br />
                        специалиста
                    </div>
                </div>
            </div>
        </div>

        <div class="row callback-row">
            <div class="col-md-6">
                <div class="left-side flex-center wow slideInUp">
                    <div>
                        <span class="title">Консультация наших специалистов, бесплатно!</span>
                        <br />
                        <span class="title">Мы поможем расчитать стоимость, посоветуем какие материалы выбрать.</span>
                        <div class="suggest-to-call-wrapper">
                                    <span class="suggest-to-call">
                                        позвоните по номеру
                                        <span class="phone"><a href="tel:+380970775706">+38 (097) 077-57-06</a></span>
                                    </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="callback flex-center wow slideInUp">
                    <div>
                        <span class="title">Или оставте ваш номер и мы перезвоним вам в течении часа</span>
                        <form class="callback-form">
                            <input class="phone-input" type="text" placeholder="ВАШ НОМЕР ТЕЛЕФОНА"/>
                            <button type="submit">ПЕРЕЗВОНИТЬ</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-12"><h1>НАШИ РАБОТЫ</h1></div>
        </div>
        <div class="slider">

        </div>
        <div class="row counters">
            <div class="col-6 col-md-3 flex-center">
                <div>
                    <div class="count"><span class="wow countup">6</span> лет</div>
                    <div class="title">Опыта<br /> работы с металом</div>
                </div>
            </div>
            <div class="col-6 col-md-3 flex-center">
                <div>
                    <div class="count"><span class="wow countup">400</span>+</div>
                    <div class="title">Довольных<br />клиентов</div>
                </div>
            </div>
            <div class="col-6 col-md-3 flex-center">
                <div>
                    <div class="count"><span class="wow countup">99</span>%</div>
                    <div class="title">Клиентов обращаются<br />к нам сново</div>
                </div>
            </div>
            <div class="col-6 col-md-3 flex-center">
                <div>
                    <div class="count"><span class="wow countup">100</span></div>
                    <div class="title">Выполненых<br /> изделий</div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="clients">
    <div class="container">
        <div class="row">
            <div class="col-md-12"><h1>КЛИЕНТЫ</h1></div>
        </div>
        <div class="comments">
            <div class="owl-carousel">
                <div class="item row">
                    <div class="col-md-4 img-block">
                        <div class="img-wrapper">
                            <img src="{{ asset('images/home-page/comments/photo-1.jpg') }}">
                            <span class="d-none d-xl-inline"><br /></span>
                            <span class="name">Вадим Курнули</span>
                            <br />
                            <span class="position">Директор магазина</span>
                        </div>
                    </div>
                    <div class="col-md-8 flex-center text">
                        <div>
                            Изумительное взаимопонимание в работе, свело на «нет» с
                            и превратило весь процесс в творчество! Не прилагая никаких
                            получили готовый продукт — сайт, который не просто отве
                            требованиям, а превзошел наши ожидания. Значительно ускор
                            есс отсутствие «десяти» не нужных версий сайта! Была сраз
                            концепция, которая полностью соответствовала нашим биз
                            Понимание с «полуслова»
                        </div>
                    </div>
                </div>

                <div class="item row">
                    <div class="col-md-4 img-block">
                        <div class="img-wrapper">
                            <img src="{{ asset('images/home-page/comments/photo-1.jpg') }}">
                            <span class="d-none d-xl-inline"><br /></span>
                            <span class="name">Вадим Курнули</span>
                            <br />
                            <span class="position">Директор магазина</span>
                        </div>
                    </div>
                    <div class="col-md-8 flex-center text">
                        <div>
                            Изумительное взаимопонимание в работе, свело на «нет» с
                            и превратило весь процесс в творчество! Не прилагая никаких
                            получили готовый продукт — сайт, который не просто отве
                            требованиям, а превзошел наши ожидания. Значительно ускор
                            есс отсутствие «десяти» не нужных версий сайта! Была сраз
                            концепция, которая полностью соответствовала нашим биз
                            Понимание с «полуслова»
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="contacts">
    <div class="container">
        <div class="row">
            <div class="col-md-6 flex-center">
                <div class="callback-wrapper wow fadeInUp">
                    <div class="callback flex-center">
                        <div>
                            <span class="title">Оставте ваш номер и мы перезвоним вам в течении часа</span>
                            <form class="callback-form">
                                <input class="phone-input" type="text" placeholder="ВАШ НОМЕР ТЕЛЕФОНА"/>
                                <button type="submit">ПЕРЕЗВОНИТЬ</button>
                            </form>
                        </div>
                    </div>

                    <div class="flex-center">
                        <div class="phone-form">
                            <span>Или позвоните по номеру:</span>
                            <span class="phone"><a href="tel:+380970775706">+38 (097) 077-57-06</a></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 flex-center">
                <div class="text wow fadeInUp">
                    <span>ДНЕПР</span>
                    <span>
                                Изумительное взаимопонимание в работе <span class="d-none d-xl-inline"><br /></span>
                                и превратило весь процесс в творчество! <span class="d-none d-xl-inline"><br /></span>
                                получили готовый продукт — сайт, кото <span class="d-none d-xl-inline"><br /></span>
                                требованиям, а превзошел наолучили
                            </span>
                    <span>Днепровская область</span>
                </div>
            </div>
        </div>
    </div>
</section>

<footer class="d-xl-flex">
    <div class="year">2012 - {{ date('Y') }} sk-railing.com.ua</div>
    <div class="logo"><a href="/">SK&RAILING</a></div>
    <div class="phone"><a href="tel:+74957322984">+38 (097) 077-57-06</a></div>
</footer>

<div id="to-top" class="d-none d-xl-block" href="#main">
    <i class="fa fa-angle-up"></i><br />
    наверх
</div>

<div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="confirmationModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Спасибо!</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
</div>
<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
