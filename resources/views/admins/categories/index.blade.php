@extends('admins.layouts.app')



@section('content')


    <div class="col-12 col-md-9">
        <div class="row">
            <div class="col-md-12">
                <h2 class="m-2 float-left text-uppercase">{{$pageName}}</h2>
                <a class="m-2 float-right btn btn-success" href="{{route('categories.create')}}">Create new Category</a>
            </div>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Order</th>
                    <th width="280px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($categories as $category)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td><a href="{{route('categories.show',$category->id)}}">{{$category->name}}</a></td>
                        <td>{{$category->ordered}}</td>
                        <td>
                            <a href="{{route('categories.show',$category->id)}}" class="btn btn-primary">Show</a>
                            <a href="{{route('categories.edit',$category->id)}}" class="btn btn-warning">Edit</a>
                            {{Form::open(['url'=>route('categories.destroy',$category->id),'method'=>'delete','style'=>'display:inline'])}}
                                {{Form::submit('Delete',['class'=>'btn btn-danger'])}}
                            {{Form::close()}}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4"><p class="text-center text-uppercase m-5">No Categories... <a
                                    href="{{route('categories.create')}}">Create?</a></p></td>
                    </tr>
                @endforelse

                </tbody>
            </table>
        </div>
    </div>

@stop
