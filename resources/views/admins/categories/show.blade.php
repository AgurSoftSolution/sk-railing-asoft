@extends('admins.layouts.app')



@section('content')


    <div class="col-12 col-md-9">
        <div class="row">
            <div class="col-md-12">
                <a class="m-2 float-left btn btn-primary" href="{{route('categories.index')}}">Back</a>
            </div>
            <div class="col-md-12">
            <h3>
                Name: {{$category->name}}
            </h3>

                <div>
                    {!! $category->description  !!}
                </div>

            <h4>
                Order: {{$category->ordered}}
            </h4>
            </div>
        </div>
    </div>

@stop
