@extends('admins.layouts.app')

@section('content')

    <div class="col-12 col-md-9">
        <div class="row">
            <div class="col-md-12">
                <a class="m-2 float-left btn btn-primary" href="{{route('categories.index')}}">Back</a>
            </div>
            <div class="col-md-5">
                {{Form::open(['url'=>route('categories.store'),'method'=>'post'])}}
                <div class="form-group">
                    {{Form::label('name','Название Категории')}}
                    {{Form::text('name',old('name'),['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('ordered','Позиция Категории')}}
                    {{Form::text('ordered',old('ordered'),['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('description','Описание Товара')}}
                    <textarea name="description" class="form-control" id="" cols="30" rows="10"></textarea>
                </div>
                {{Form::submit('Сохранить Категорию',['class'=>'btn btn-success'])}}
                {{Form::close()}}
            </div>
        </div>
    </div>

@stop
