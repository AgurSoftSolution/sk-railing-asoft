@extends('admins.layouts.app')

@section('content')

    <div class="col-12 col-md-9">
        <div class="row">
            <div class="col-md-12">
                <a class="m-2 float-left btn btn-primary" href="{{route('products.index')}}">Back</a>
            </div>
            <div class="col-md-12">
                {{Form::open(['url'=>route('products.store'),'method'=>'post','files'=>true])}}
                <div class="form-group">
                    {{Form::label('category','Категории')}}
                    {{Form::select('category', $categories,$categories,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('main_image','Главное Изображение товара')}}
                    {{Form::file('main_image',['class'=>"form-control",'multiple' => false])}}
                </div>
                <div class="form-group">
                    {{Form::label('gallery','Галерея')}}
                    {{Form::file('gallery[]',['class'=>"form-control",'multiple' => true])}}
                </div>
                <div class="form-control-file"></div>
                <div class="form-group">
                    {{Form::label('name','Название Товара')}}
                    {{Form::text('title',old('title'),['class'=>'form-control'])}}
                </div>

                <div class="form-group">
                    {{Form::label('ordered','Код Товара')}}
                    {{Form::text('code',old('code'),['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('price','Цена Товара')}}
                    {{Form::number('price',old('price'),['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('description','Описание Товара')}}
                    <textarea name="description" class="form-control" id="" cols="30" rows="10"></textarea>
                </div>
                {{Form::submit('Сохранить Товар',['class'=>'btn btn-success'])}}
                {{Form::close()}}
            </div>
        </div>
    </div>

@stop
