@extends('admins.layouts.app')



@section('content')
    <div class="col-12 col-md-9">
        <div class="row">
            <div class="col-md-12">
                <a class="m-2 float-left btn btn-primary" href="{{route('products.index')}}">Back</a>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <strong>Название:</strong>
                        <h2 class="font-weight-bold">{{$product->title}}</h2>
                        <strong>Цена:</strong><h4 class="font-weight-bold">{{$product->price}}</h4>
                        <strong>Код:</strong><h4 class="font-weight-bold">{{$product->code}}</h4>
                        <div class="col-md-12">
                            <div class="text-left">
                                {!! $product->description  !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img width="375px" src="{{getImage($product->main_image)}}" alt="{{$product->title}}">
                        <div class="col-md-12">
                            <div class="row">
                                @forelse($product->gallery as $image)
                                    <div class="col-md-4">
                                        <img width="90px" src="{{getImage($image->filename)}}" alt="{{$product->title}}">
                                    </div>
                                @empty
                                    Галереи не существует
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
