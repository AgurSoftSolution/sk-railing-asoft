@extends('admins.layouts.app')



@section('content')

</span>
    <div class="col-12 col-md-9">
        <div class="row">
            <div class="col-md-12">
                <h2 class="m-2 float-left text-uppercase">{{$pageName}}</h2>
                <a class="m-2 float-right btn btn-success" href="{{route('products.create')}}">Создать новый товар</a>
            </div>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Code</th>
                    <th width="280px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($products as $product)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td><a href="{{route('products.show',$product->id)}}">{{$product->title}}</a></td>
                        <td>
                            <img width="100px" src="{{getImage($product->main_image)}}" alt="{{$product->title}}">
                        </td>
                        <td><a href="{{route('categories.show',$product->categories->id)}}">{{$product->categories->name}}</a></td>
                        <td>{{$product->price}}/td>
                        <td>{{$product->code}}</td>
                        <td>
                            <a href="{{route('products.show',$product->id)}}" class="btn btn-primary">Show</a>
                            <a href="{{route('products.edit',$product->id)}}" class="btn btn-warning">Edit</a>
                            {{Form::open(['url'=>route('products.destroy',$product->id),'method'=>'delete','style'=>'display:inline'])}}
                                {{Form::submit('Delete',['class'=>'btn btn-danger'])}}
                            {{Form::close()}}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7"><p class="text-center text-uppercase m-5">No products... <a
                                    href="{{route('products.create')}}">Create?</a></p></td>
                    </tr>
                @endforelse

                </tbody>
            </table>
        </div>
    </div>

@stop
