@extends('admins.layouts.app')

@section('content')

    <div class="col-12 col-md-9">
        <div class="row">
            <div class="col-md-12">
                <a class="m-2 float-left btn btn-primary" href="{{route('products.index')}}">Back</a>
            </div>
            <div class="col-md-12">
                {{Form::open(['url'=>route('products.update',$product->id),'method'=>'put','files'=>true])}}
                <div class="form-group">
                    {{Form::label('category','Категории')}}
                    {{Form::select('category', $categories,$product->category_id?$product->category_id:old('category_id'),['class'=>'form-control'])}}
                </div>
                <div class="row">
                    <div class="form-group col-md-5">
                        {{Form::label('main_image','Главное Изображение товара')}}
                        {{Form::file('main_image',['class'=>"form-control",'multiple' => false])}}
                    </div>
                    <div class="form-group col-md-6">
                        <img class="col-md-12" src="{{getImage($product->main_image)}}" alt="">
                    </div>
                </div>


                <div class="form-group">
                    {{Form::label('title','Название Товара')}}
                    {{Form::text('title',$product->title,['class'=>'form-control'])}}
                </div>

                <div class="form-group">
                    {{Form::label('code','Код Товара')}}
                    {{Form::text('code',$product->code,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('price','Цена Товара')}}
                    {{Form::number('price',$product->price,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::label('description','Описание Товара')}}
                    <textarea name="description" class="form-control" id="description" cols="30"
                              rows="10">{{$product->description}}</textarea>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <div class="form-group col-md-5">
                            {{Form::label('gallery','Галерея товара')}}
                            {{Form::file('gallery[]',['class'=>"form-control",'multiple' => true])}}
                        </div>
                        <div class="form-group col-md-5">
                            {{Form::submit('Сохранить Товар',['class'=>'btn btn-success'])}}
                            {{Form::close()}}
                        </div>

                    </div>
                    <div class="form-group col-md-6">
                        <div class="row">

                                    <div class="row">
                                        @forelse($product->gallery as $image)
                                            <div class="col-md-4">
                                                <img width="90px" src="{{getImage($image->filename)}}"
                                                     alt="{{$product->title}}">
                                                {{Form::open(['url'=>route('remove.gallery.item',[$image->product_id,$image->id]),'method'=>'delete'])}}
                                                {{Form::submit('X',['class'=>'btn btn-sm btn-danger'])}}
                                                {{Form::close()}}

                                            </div>
                                        @empty
                                            Галереи не существует
                                        @endforelse
                                    </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>

@stop
