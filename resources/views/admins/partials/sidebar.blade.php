<div class="col-6 col-md-3 sidebar-offcanvas" id="sidebar">
    <div class="list-group">
        <a href="{{route('products.index')}}" class="list-group-item {{isActiveRoute('products.index')}} ">Товар</a>
        <a href="{{route('categories.index')}}"
           class="list-group-item {{isActiveRoute('categories.index')}} ">Категории</a>
        <a href="{{route('admin.callback')}}" class="list-group-item {{isActiveRoute('admin.callback')}} ">Перезвонить</a>
    </div>
</div><!--/span-->
