@if(isset($errors))
    <div class="container">
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger">
            <strong>Внимание!</strong>
            {{$error }}
        </div>
    @endforeach
    </div>
@endif
