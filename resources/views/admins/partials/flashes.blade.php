@if(Session::has('message'))
    <div class="container">
        <div class="alert alert-primary">
            <strong>Внимание!</strong>
            {{ Session::get('message') }}
        </div>
    </div>
@endif
