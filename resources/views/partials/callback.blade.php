

<div class="callback flex-center">

    <div>
        <span class="title">Оставте ваш номер и мы перезвоним вам в течении часа</span>
        {{Form::open(['url'=>route('callback'),'method'=>'post'])}}
            {{Form::text('phone_number', null ,['placeholder'=>'ВАШ НОМЕР ТЕЛЕФОНА', 'class'=>'phone-input'])}}
            <button type="submit">ПЕРЕЗВОНИТЬ</button>
        {{Form::close()}}
    </div>

</div>
