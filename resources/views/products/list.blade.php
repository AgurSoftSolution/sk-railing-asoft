@extends('layouts.app')
@section('breadcrumbs')
    {{ Breadcrumbs::render('category') }}
@stop
@section('description')
    @include('partials.callback')
@endsection
@section('content')
    <div class="container">
        <div class="row row-offcanvas row-offcanvas-left">
            <div class="col-6 col-md-3 sidebar-offcanvas" id="sidebar">
                <div class="list-group">


                    @foreach ($categories as $category)
                        <a href="{{route('products.by.category',$category->id)}}"
                           class="list-group-item ">{{ $category->name }}</a>
                    @endforeach


                </div>
            </div><!--/span-->
            <div class="col-12 col-md-9">
                <div class="row">
                        @foreach ($products as $product)
                    <div class="col-6 col-lg-4">
                            <a href="{{route('product.by.id',$product->id)}}">
                                <div class="card">
                                    <img class="card-img-top" src="{{getImage($product->main_image)}}"
                                         alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$product->title}}</h5>
                                    </div>
                                </div>
                            </a>
                    </div>
                        @endforeach
                </div>
                        {{ $products->links() }}
            </div>

        </div>
    </div>
    <div style="text-align: justify;" class="container py-5 ">
        <div class="row">
            <h2 style="margin: 10px auto;" class="text-uppercase">
                {{Session::get('category')->name}}
            </h2>
            <p>
              {!! Session::get('category')->description !!}
            </p>
        </div>
    </div>
@endsection
