@extends('layouts.app')
<link rel="stylesheet" type="text/css" href="/css/demo.css" />
<link rel="stylesheet" type="text/css" href="/css/style.css" />
<link rel="stylesheet" type="text/css" href="/css/elastislide.css" />
@section('breadcrumbs')
@section('breadcrumbs')
    {{ Breadcrumbs::render('product', $product) }}
@stop
@stop
@section('description')
    @include('partials.callback')
@endsection
@section('content')
    <div class="container">
        <div class="row row-offcanvas row-offcanvas-left">
            <div class="col-6 col-md-3 sidebar-offcanvas" id="sidebar">
                <div class="list-group">
                    @foreach ($categories as $category)
                        <a href="{{route('products.by.category',$category->id)}}"
                           class="list-group-item">{{ $category->name }}</a>
                    @endforeach
                </div>
            </div><!--/span-->
            <div class="col-12 col-md-9">
                <div class="row">
                    <div class="card">
                        <div class="card-body">
                            <div style="float: left;width: 50%;">
                                <script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">
                                    <div class="rg-image-wrapper">
                                        <div class="rg-image-nav">
                                            <a href="#" class="rg-image-nav-prev">Предыдущее изображение</a>
                                            <a href="#" class="rg-image-nav-next">Следующее изображение</a>
                                        </div>
                                        <div class="rg-image"></div>
                                        <div class="rg-loading"></div>
                                        <div class="rg-caption-wrapper">
                                            <div class="rg-caption" style="display:none;">
                                                <p></p>
                                            </div>
                                        </div>
                                    </div>
                                </script>
                                {{--<img class="card-img-top" src="{{getImage($product->main_image)}}" alt="{{$product->title}}">--}}
                                <div class="row">
                                    <div class="es-carousel-wrapper">
                                        <div class="es-nav">
                                            <span class="es-nav-prev">Предыдущее</span>
                                            <span class="es-nav-next">Следующее</span>
                                        </div>
                                        <div class="es-carousel">
                                            <ul>
                                                @foreach($product->gallery as $image)
                                                    <li><a href="#"><img src="{{getImage($image->filename)}}" data-large="{{getImage($image->filename)}}" alt="{{$product->title}}" data-description="{{$product->title}}" /></a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div >
                                <h5 class="card-title">{{$product->title}}</h5>
                                <p class="card-text">{!! $product->description !!}</p>
                            </div>
                            <p style=" width:100% ; text-align: right;">
                                <span>Цена:<b>{{$product->price}}</b></span>
                                <span>Код товара:<b>{{$product->code}}</b></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    {{--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>--}}
    <script type="text/javascript" src="/js/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="/js/jquery.elastislide.js"></script>
    <script type="text/javascript" src="/js/gallery.js"></script>

@endsection
