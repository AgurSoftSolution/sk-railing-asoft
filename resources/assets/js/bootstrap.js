
window._ = require('lodash');
window.Popper = require('popper.js').default;

import 'owl.carousel/dist/assets/owl.carousel.css';

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
    require('owl.carousel');

    $('.owl-carousel').owlCarousel({
        items: 1,
        loop: true,
        center: true,
        nav: true,
        dots: false
    });

    $('#to-top, menu a, navbar-nav a').click(function () {
        let anchor = $(this).attr('href');
        $("html, body").animate({scrollTop: $(anchor).offset().top - 40}, 500);
    });
} catch (e) {}


import wow from 'wow.js';
new wow().init();

import CountUp from 'countup.js';

let easingFn = function (t, b, c, d) {
    let ts = (t /= d) * t;
    let tc = ts * t;

    return b + c * (tc + -3 * ts + 3 * t);
};

let options = {
    useEasing: true,
    easingFn: easingFn,
};

$('.countup').each(function () {
    let observer = new MutationObserver((mutationsList) => {
        for (let mutation of mutationsList) {
            if (mutation.type == 'attributes' && mutation.attributeName == 'class') {
                new CountUp(this, 0, $(this).text(), 0, 1.5, options).start();
            }
        }
    });

    observer.observe(this, {attributes: true});
});

function affix() {
    let scrollValue = $(window).scrollTop();
    if (scrollValue > 70) {
        $('header').addClass('sticked');
    } else {
        $('header').removeClass('sticked');
    }
}

$(window).on('scroll', function(event) {
    affix();
});

$('document').ready(function () {
    affix();
});

$('.navbar-toggler').click(function () {
    if ($('.navbar .logo').hasClass('collapsed')) {
        $('.navbar .logo').removeClass('collapsed');
    } else {
        $('.navbar .logo').addClass('collapsed');
    }
});

import Inputmask from "inputmask";

$('.phone-input').each(function () {
    let im = new Inputmask("+38 (999) 999-99-99");
    im.mask(this);
});

require('jquery-form');

$('.callback-form').on('submit', function (e) {
    e.preventDefault();

    let phone = $(this).find('.phone-input').val();

    if (!phone.length || phone.indexOf('_') > 0) {
        alert('Error');
        return false;
    }

    $('#confirmationModal .modal-body').html("Мы вам перезвоним в течении часа на номер <br /><b>" + phone + "</b>");
    $('#confirmationModal').modal();
});

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

// window.axios = require('axios');
//
// window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
//
// /**
//  * Next we will register the CSRF Token as a common header with Axios so that
//  * all outgoing HTTP requests automatically have it attached. This is just
//  * a simple convenience so we don't have to attach every token manually.
//  */
//
// let token = document.head.querySelector('meta[name="csrf-token"]');
//
// if (token) {
//     window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
// } else {
//     console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
// }

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
