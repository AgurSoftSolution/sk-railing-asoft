<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App
 * @property $title;
 * @property $content;
 * @property $code;
 * @property $price;
 * @property $description;
 */
class Product extends Model
{
    protected $fillable = [
        'title',
        'main_image',
        'description',
        'code',
        'user_id',
        'price',
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function categories()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function gallery()
    {
        return $this->hasMany(Gallery::class);
    }
}
