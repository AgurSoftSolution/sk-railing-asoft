<?php

namespace App\Http\Controllers;

use App\Callback;
use App\Category;
use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function callback(Request $request)
    {
        Callback::create($request->all());
        return redirect()->back();
    }

    public function index()
    {
        return view('home');
    }

    public function productsByCategory(Category $category)
    {
        \Session::put('category', $category);
        $categories = Category::all();
        $products = $category->products()->paginate(9);
        return view('products.list', compact('categories', 'products'));
    }

    public function productById(Product $product)
    {
        $product->load('gallery');
        $categories = Category::all();
        return view('products.item', compact('product', 'categories'));
    }

    public function products()
    {
        $categories = Category::all();
        $products = Product::paginate(9);
        return view('products.list', compact('categories', 'products'));
    }
}
