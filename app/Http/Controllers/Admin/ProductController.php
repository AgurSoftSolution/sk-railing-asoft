<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Gallery;
use App\Http\Requests\ProductCreateRequest;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    private $pageName = "Товары";

    public function index()
    {
        $pageName = $this->pageName;
        $products = Product::with('categories')->get();
        return view('admins.products.index', compact('pageName', 'products'));
    }


    public function create()
    {
        $categories = self::getCategories();
        return view('admins.products.create', compact('categories'));
    }

    public static function SaveImage($image, $folder = 'product_images', $disk = 'public')
    {
        return \Storage::disk($disk)->put($folder, $image);
    }

    public function store(ProductCreateRequest $request)
    {

        /**
         * @var Category $category
         */
        $category = Category::getCategory($request);
        $request->main_image = self::SaveImage($request->file('main_image'));

        $result = $category->products()->create([
            'user_id' => \Auth::id(),
            'title' => $request->title,
            'main_image' => $request->main_image,
            'price' => $request->price,
            'code' => $request->code,
            'description' => $request->description,
        ]);

        foreach ($request->file('gallery') as $file) {
            $gallery['product_id'] = $result->id;
            $gallery['filename'] = self::SaveImage($file, 'product_images/gallery');
            Gallery::create($gallery);
        }

        \Session::flash('message', 'Товар был создан успешно');
        return redirect()->route('products.index');
    }

    public function show(Product $product)
    {

        $product = $product->load('categories', 'gallery');
        return view('admins.products.show', compact('product'));
    }


    public function edit(Product $product)
    {
        $product = $product->with('gallery')->first();
        $categories = self::getCategories();
        return view('admins.products.edit', compact('categories', 'product'));
    }

    public static function getCategories()
    {
        return Category::get(['name', 'id'])->pluck('name', 'id')->toArray();

    }

    public function removeItem($product, $gallery)
    {
        try {
            $product = Product::find($product)->with('gallery')->get()->first();
            $image = $product->gallery->where('id', $gallery)->first();
            $image_path = $image->filename;
            $image->delete();
            Storage::disk('public')->delete($image_path);
            return redirect()->back();
        } catch (\Error $error) {
            \Session::flash('error', $error);
        }
    }

    public static function deleteImage($file)
    {
        Storage::disk('public')->delete($file);
    }

    public function update(Request $request, Product $product)
    {
        /**
         * @var Category $category
         */
        $category = Category::getCategory($request);
        if ($request->file('main_image')) {
            $image = self::SaveImage($request->file('main_image'));
            self::deleteImage($product->main_image);
            $request->main_image = $image;
        }
        $product->category_id = $category->id;
        $product->update([
            'title' => $request->title,
            'main_image' => $request->main_image,
            'price' => $request->price,
            'code' => $request->code,
            'description' => $request->description,
        ]);
        if ($request->file('gallery')) {
            foreach ($request->file('gallery') as $file) {
                $gallery['product_id'] = $product->id;
                $gallery['filename'] = self::SaveImage($file, 'product_images/gallery');
                Gallery::create($gallery);
            }
        }
        return redirect()->back();
    }


    public function destroy(Product $product)
    {
        $image = $product->main_image;
        $result = $product->delete();
        if ($result) {
            Storage::disk('public')->delete($image);
        }
        return redirect()->route('products.index');
    }
}
