<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\CategoryCreateRequest;
use App\Http\Requests\CategoryEditRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    private $pageName = "Категории";


    public function index()
    {
        $pageName = $this->pageName;
        $categories = Category::getOrdered();
        return view('admins.categories.index', compact('categories', 'pageName'));
    }

    public function create()
    {
        return view('admins.categories.create');
    }

    public function store(CategoryCreateRequest $request)
    {
        $category = Category::checkIfExistByName($request->name)->first();
        Category::createOrUpdate($category, $request);
        return redirect()->route('categories.index');
    }


    public function show(Category $category)
    {
        return view('admins.categories.show', compact('category'));
    }

    public function edit(Category $category)
    {
        return view('admins.categories.edit', compact('category'));
    }


    public function update(Request $request, Category $category)
    {
        Category::createOrUpdate($category, $request);
        return redirect()->route('categories.index');
    }

    public function destroy(Category $category)
    {
        $category->delete();
        Session::flash('message', 'Категория была удалена успешно');
        return redirect()->route('categories.index');
    }
}
