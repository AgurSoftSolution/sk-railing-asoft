<?php

namespace App\Http\Controllers\Admin;

use App\Callback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CallbackController extends Controller
{
    private $pageName = 'Перезвонить';

    public function index()
    {
        $pageName = $this->pageName;
        $callbacks = Callback::all();
        return view('admins.callback.index', compact('callbacks','pageName'));
    }
}
