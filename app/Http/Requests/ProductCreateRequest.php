<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


/***
 * Class ProductCreateRequest
 * @package App\Http\Requests
 * @property $title
 * @property $main_image
 * @property $price
 * @property $code
 * @property $content
 */
class ProductCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'main_image' => 'required|mimes:jpg,png,jpeg,bmp|max:3000',
            'gallery' => 'required',
            'gallery.*' => 'required|mimes:jpg,png,jpeg,bmp|max:3000',
            'title' => 'required|string',
            'price' => 'required',
            'code' => 'required',
            'description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'main_image.required' => 'Главное изображение должно быть загружено',
            'main_image.*' => 'Возникли проблеммы при загрузке файлов',
            'gallery.*' => 'Возникли проблеммы при загрузке файлов',
            'main_image.mimes' => 'Главное изображение должно быть такого формата:jpg,png,jpeg,bmp',
            'main_image.max' => 'Главное изображение должно весить меньше 6 мб',
            'title.required' => 'Название товара должно быть заполненым',
            'price.required' => 'Цена товара должна быть заполненым',
            'code.required' => 'Код товара должно быть заполненым',
            'description.required' => 'Информация о товаре должна быть заполненым',
            'category.required' => 'Вы должны выбрать категорию',
            'gallery.required' => 'Вы должны загрузить изображения для Галереи',
            'gallery.*.required' => 'Вы должны загрузить изображения для Галереи',
            'gallery.*.mimes' => 'Изображения должны быть такого формата:jpg,png,jpeg,bmp',
        ];
    }
}
