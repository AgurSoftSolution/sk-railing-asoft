<?php

namespace App;

use App\Http\Requests\CategoryCreateRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Category extends Model
{

    const SORT_BY_DESC = 'desc';
    const SORT_ID_COLUMN = 'id';
    const SORT_NAME_COLUMN = 'name';
    const SORT_BY_ASC = 'asc';

    public $primaryKey = 'id';
    protected $fillable = [
        'name',
        'ordered',
        'description'
    ];

    public static function checkIfExistByName($name)
    {
        return self::where('name', '=', $name);
    }

    /**
     * @param self $category
     * @param Request $request
     */
    public static function createOrUpdate($category, $request)
    {
        $data = $request->only(['name', 'ordered','description']);
        if ($category) {
            $newCategory = $category->update($data);
            Session::flash('message', "Категория " . $newCategory['name'] . " Обновлена");

        } else {
            $category = Category::create($data);
            Session::flash('message', "Категория" . $category->name . "была успешно создана");
        }
    }

    public static function getCategory($request)
    {
        return self::find($request->get('category'));
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public static function getOrdered($order = 'asc', $column = 'ordered')
    {
        return self::orderBy($column, $order)->get();
    }

}
