<?php

//Главная
Breadcrumbs::for('home', function ($trails) {
    $trails->push('Главная', route('home'));
});

//Главная > Категория

Breadcrumbs::for('category', function ($trails) {
    $trails->parent('home');
    if (Session::has('category')) {
        $category = Session::get('category');
        $trails->push($category->name, route('products.by.category', $category->id));
    }
});

//Главная > Категория

Breadcrumbs::for('product', function ($trails, \App\Product $product) {
    $trails->parent('category');
    $trails->push($product->title, route('products.by.category', $product->id));
});


