<?php

Route::group(['prefix' => 'admin','namespace'=>'Admin','middleware'=>['web','auth']], function () {
    Route::get('/', 'HomeController@index')->name('admin.home');
    Route::resource('categories','CategoryController');
    Route::resource('products','ProductController');
    Route::get('callback','CallbackController@index')->name('admin.callback');
    Route::delete('products/remove_item/{product}/{gallery}','ProductController@removeItem')->name('remove.gallery.item');
});
    Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::post('/callback', 'HomeController@callback')->name('callback');
Route::get('/products', 'HomeController@products')->name('products');
Route::get('/product/{product}', 'HomeController@productById')->name('product.by.id');
Route::get('/products/{category}', 'HomeController@productsByCategory')->name('products.by.category');

