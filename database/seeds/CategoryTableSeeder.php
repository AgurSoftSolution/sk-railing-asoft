<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Жалюзи',
                'ordered' => '1'
            ],
            [
                'name' => 'Перила',
                'ordered' => '4'
            ],
            [
                'name' => 'Ограждения',
                'ordered' => '2'
            ],
            [
                'name' => 'Двери',
                'ordered' => '5'
            ],
            [
                'name' => 'Козырьки',
                'ordered' => '3'
            ],
        ];

        foreach ($categories as $category) {
            $result = Category::create($category);
        }
    }
}
