<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{

    public function run()
    {

        /**
         * Generate Main User (admin)
         */

        $user = [
            'name'=>'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make(24049898)
        ];

        User::create($user);
    }
}
